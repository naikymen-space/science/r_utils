## Module2
a <- rnorm(20)
plot(a,col='red')
hist(a)
plot(density(rnorm(1000)))
?density

library(flowCore)
setwd('/media/naikymen/SD64/Tesina/Resultados/Citometria/')
f <- read.FCS('Sin tranfectar.FCS')
colnames(f)
f@description
f@parameters@data

library(flowViz)
plot(f, c("FSC", "SSC"), smooth=FALSE)

fs <- read.flowSet(path = "/media/naikymen/SD64/Tesina/Resultados/Citometria/", pattern = ".FCS")
fs
sampleNames(fs)
length(fs)


flowFrame()

nrow(fs[[1]])
fsApply(fs, nrow)
fsApply(fs, function(f) f@description$'TUBE NAME')

## Fin, a experimentar a mano!
fs
f=fs[[13]]
f
E <- exprs(f)  # obtener los valores
dim(E)  # dimensiones de E
E[1:10, ]
which(E[, 1] > 0)
intersect(which(E[, 1] > 0), which(E[, 2] > 0))
# ahora me tocaría poder sacar los valores que corresponden a esos índices

plot(f, c("FSC", "SSC"), smooth=FALSE,log='x')  # log solo x
plot(f, c("FSC", "SSC"), smooth=FALSE,log='')  # lineal
plot(f, c("FSC", "SSC"), smooth=FALSE,log='xy',xlim=c(1,500),ylim=c(1,500),main='Wild type')  # x e y

f=fs[[4]]
f
plot(f, c("FSC", "SSC"), smooth=FALSE,log='xy',xlim=c(1,500),ylim=c(1,500),main='Clon 1 GFPRab11')  # x e y

f=fs[[2]]
plot(f, c("FSC", "SSC"), smooth=FALSE,log='xy',xlim=c(1,500),ylim=c(1,500),main='Clon Rab11GFP')  # x e y
