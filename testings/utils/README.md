# Easy save RDS

* https://raw.githubusercontent.com/rstudio/cheatsheets/master/tidyeval.pdf
* https://rlang.r-lib.org/reference/as_string.html
* https://stackoverflow.com/questions/57960245/what-is-the-difference-between-ensym-and-enquo-when-programming-with-dplyr

```
save_rds_easy <- function(object, folder){
  # https://github.com/naikymen/R-simple-tools
  nombre_del_objeto <- rlang::as_string(rlang::ensym(object))
  
  if(nombre_del_objeto == ".")
    stop("Do not pipe the object to save, this uses the object's name to build the file name.")
  
  archivo <- tempfile(tmpdir = folder, fileext = ".RDS", 
                      pattern = paste0(nombre_del_objeto, "_", format(Sys.time(), "%Y%m%d"), "_"))
  
  print(paste0("Archivo guardado en: ", archivo), quote = F)
  
  saveRDS(object, archivo)
}
``` 

# all.distinct

```
all.distinct <- function(...){
	return(length(unique(c(...))) == length(c(...)))
}
```
