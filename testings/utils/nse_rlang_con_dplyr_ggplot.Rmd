---
title: "dplyr y ggplot con variables en strings (NSE y rlang)"
output: html_notebook
---

Tenemos variables de interés guardadas en strings y queremos usarlas en ggplot o dplyr:

```{r}
x = "cyl"
y = "mpg"
```

Converting strings to symbols is easy

```{r}
x_ = rlang::parse_expr(x)
y_ = rlang::parse_expr(y)
```


ggplot needs "`!!`" (*bang bang*) to evaluate symbols:

```{r}
ggplot2::ggplot(mtcars) + 
  ggplot2::geom_point(ggplot2::aes(x = !!x_,
                                   y = !!y_))
```

`dplyr::rename` needs "`:=`" instead of the bare "`=`" to behave as expected:

```{r}
dplyr::rename(iris,
              !!x_ := Sepal.Length,
              !!y_ := Sepal.Width)

```

A similar thing is possible with lists:

```{r}
.vars = c("cyl", "mpg")
```

```{r}
vars_ = rlang::parse_exprs(.vars)
```

```{r}
dplyr::select(mtcars, !!!vars_)
```

Es interesante lo que pasa al nombrar el vector al principio:

```{r}
.vars = c(x = "cyl", y = "mpg")
vars_ = rlang::parse_exprs(.vars)
dplyr::select(mtcars, !!!vars_)
```

