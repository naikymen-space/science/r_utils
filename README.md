# library(utiles)

Desde R, la forma fácil:

```r
install_gitlab("https://gitlab.com/naikymen/r_utils")
```

Sino, clonar el repo con `git`, abrir el proyecto e instalarlo con `devtools::install()`.
